#!/bin/bash

if [ -z "$1" ] ; then
    echo 
    echo "Usage: ./retest.sh 5.0-SNAPSHOT"
    echo
    echo "Requires: confluence-perf-cac-sql-*.sql - Snapshot of the database"
    echo "Requires: confluence-perf-cac-datadir-3.2.1-1813-v2license.tar.gz - Snapshot of the home dir"
    echo "Requires: update-confluence-cfg-xml.patch"
    echo "Requires: pom-start-confluence.xml"
    echo
    exit 1;
fi

set -u
set -e

CONFLUENCE_VERSION=$1

rm -rf perf-auto-datadir/ import.log

# Recreate the database ( /!\ Ignores errors )
psql -d postgres -U postgres -f drop-and-recreate.sql
psql -U loadtest -d loadtest -f confluence-perf-cac-sql-*.sql 2>&1 | tee import.log || true

# Check out the data
tar xf confluence-perf-cac-datadir-3.2.1-1813-v2license.tar.gz

# Update confluence.cfg.xml
# cp confluence.cfg.xml perf-auto-datadir/
patch -p0 perf-auto-datadir/confluence.cfg.xml < update-confluence-cfg-xml.patch

echo "The version of Confluence is $CONFLUENCE_VERSION"

mvn clean amps:debug -f pom-start-confluence.xml -Dconfluence.version=$CONFLUENCE_VERSION # Runs Confluence
echo "If Confluence hungs at startup, do kill -9, then restart:"
echo "mvn clean amps:debug -f pom-start-confluence.xml -Dconfluence.version=$CONFLUENCE_VERSION"

